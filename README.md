# B2 Cron Backup Script

A script for using the Backblaze B2 cloud backup service, to be used by cron.

Credit for original script belongs to Logan Marchione[1].  I modified his
script to include a JSON-based configuration structure.

## Usage

1. Clone repository
2. Create your own setup file by removing ".example" from script-config.example.json (make sure your file is not readable by anyone whom you don't want reading it, on your system)
3. Populate your config file with your B2 values
4. Run backup.sh from your shell, or configure cron to run it for you.

### References

1. "Backblaze B2 backup setup," Logan Marchione, 2017/07/06, https://www.loganmarchione.com/2017/07/backblaze-b2-backup-setup/