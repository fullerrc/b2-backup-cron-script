#!/bin/bash

CONFIG=`cat ./script-config.example.json | jq '.'`

if [[ ! -f ./script-config.json ]]
then
    echo "No local configuration found.  Using example file."
    echo "Note: This probably won't work."
else
    CONFIG=`cat ./script-config.json | jq '.'`
fi

# Backblaze B2 configuration variables
B2_ACCOUNT="`echo $CONFIG | jq '.B2_ACCOUNT' | sed 's/\"//g'`"
B2_KEY="`echo $CONFIG | jq '.B2_KEY' | sed 's/\"//g'`"
B2_BUCKET="`echo $CONFIG | jq '.B2_BUCKET' | sed 's/\"//g'`"
B2_DIR="`echo $CONFIG | jq '.B2_DIR' | sed 's/\"//g'`"

# Local directory to backup
LOCAL_DIR="`echo $CONFIG | jq '.LOCAL_DIR' | sed 's/\"//g'`"

# GPG key (last 8 characters)
ENC_KEY="`echo $CONFIG | jq '.ENC_KEY' | sed 's/\"//g'`"
SGN_KEY="`echo $CONFIG | jq '.SGN_KEY' | sed 's/\"//g'`"
export PASSPHRASE="`echo $CONFIG | jq '.PASSPHRASE' | sed 's/\"//g'`"
export SIGN_PASSPHRASE="`echo $CONFIG | jq '.SIGN_PASSPHRASE' | sed 's/\"//g'`" 

# # Remove files older than 90 days
# duplicity \
#  --sign-key $SGN_KEY --encrypt-key $ENC_KEY \
#  remove-older-than 90D --force \
#  b2://${B2_ACCOUNT}:${B2_KEY}@${B2_BUCKET}/${B2_DIR}

# Perform the backup, make a full backup if it's been over 30 days
duplicity \
 --sign-key $SGN_KEY --encrypt-key $ENC_KEY \
 --full-if-older-than 30D \
 ${LOCAL_DIR} b2://${B2_ACCOUNT}:${B2_KEY}@${B2_BUCKET}/${B2_DIR}

# Cleanup failures
duplicity \
 cleanup --force \
 --sign-key $SGN_KEY --encrypt-key $ENC_KEY \
 b2://${B2_ACCOUNT}:${B2_KEY}@${B2_BUCKET}/${B2_DIR}

# Show collection-status
duplicity collection-status \
 --sign-key $SGN_KEY --encrypt-key $ENC_KEY \
  b2://${B2_ACCOUNT}:${B2_KEY}@${B2_BUCKET}/${B2_DIR}

# Unset variables
unset B2_ACCOUNT
unset B2_KEY
unset B2_BUCKET
unset B2_DIR
unset LOCAL_DIR
unset ENC_KEY
unset SGN_KEY
unset PASSPHRASE
unset SIGN_PASSPHRASE